<?php include('header.php')?>
	<!--Hero Section Start-->
<section class="heroSection">
  
  <div class="row no-gutters">
    <div class="col-lg-6">
      <div class="sliderBanner">
        <div class="owl-carousel owl-theme">
          <div class="item"> <img src="img/luxury_resort_and_spa-1920x1080.jpg" alt="Slider"> </div>
          <div class="item"> <img src="img/68463454-hotel-wallpapers.jpg" alt="Slider"> </div>
          <div class="item"> <img src="img/venetian_resort_hotel_casino_las_vegas-wide.jpg" alt="Slider"> </div>
          <div class="item"> <img src="img/img3.jpg" alt="Slider"> </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="heroSection_right_block">
        <div class="row no-gutters">
          <div class="col-lg-6">
            <figure> <img src="img/mardi-himal-trek2.jpg" alt="Mardi Himal">
              <figcaption> <a href="treks.php" class="btn btn-outline-secondary">Treks</a> </figcaption>
            </figure>
          </div>
          <div class="col-lg-6">
            <div class="blockSection">
              <h4>About Us</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
              <a href="about.php" class="btn btn-outline-warning">Read More</a> </div>
          </div>
          <div class="col-lg-6">
            <figure> <img src="img/world-map-146505_1280.jpg" alt="Map">
              <figcaption> <a href="contact.php" class="btn btn-outline-secondary">Contact Us</a> </figcaption>
            </figure>
          </div>
          <div class="col-lg-6">
            <figure> <img src="img/ghalelgaun.jpg" alt="Ghalel Gaun">
              <figcaption> <a href="village.php" class="btn btn-outline-secondary">Village</a> </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero Section End--> 
<!--Booking Section Start-->
<section class="bookingSection">
  <div class="row filter_room no-gutters">
    <div class="col-lg-2">
      <h2 class="bookRoom">Book Your <span>Rooms</span></h2>
    </div>
    <div class="col-lg-9">
      <div class="row">
        <div class="col-lg-3">
          <h4>Room Category</h4>
          <!-- Build your select: -->
          <select id="roomCategory" multiple="multiple">
            <option value="deluxe room">Deluxe Room</option>
            <option value="triple bed room">Triple Bed Room</option>
            <option value="Double bed room">Double Bed Room</option>
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Adults</h4>
          <!-- Build your select: -->
          <select id="adults" >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Children</h4>
          <!-- Build your select: -->
          <select id="children">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-5"> <span id="two-inputs"><span class="input-wrap">
          <h4>Check In</h4>
          <input id="date-range200" class="form-control" size="20" value="">
          </span><span class="input-wrap">
          <h4>Check Out</h4>
          <input id="date-range201" class="form-control" size="20" value="">
          </span></span> </div>
      </div>
    </div>
    <div class="col-lg-1">
      <div class="btn-wrapper">
        <button type="submit" class="btn btn-dark">Filter</button>
      </div>
    </div>
  </div>
</section>
<!--Booking Section End--> 
<!--About Section Start-->
<section class="aboutSection">
  <div class="container">
    <div class="row">
      <div class="col-lg-6"> <img src="img/22303168.jpg" alt=""> </div>
      <div class="col-lg-6">
        <div class="introBlock">
          <div class="introContent">
            <h3><span>Welcome To</span> Mardi Himal Eco Village
              Hotel & Restaurant</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
            <a href="#" class="btn btn-outline-warning">Read More</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--About Section End--> 
<!--Service Section Start-->
<section class="serviceSection graySection">
  <div class="container">
    <div class="title text-center">
      <h2>Our Services</h2>
      <p> <span>Hotel provides all services you need.</span></p>
    </div>
    <div class="serviceList clearfix">
      <ul>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/room-service.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Room Service</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/ticket.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Ticketing</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/restaurant-cutlery-circular-symbol-of-a-spoon-and-a-fork-in-a-circle.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Resturant</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/wifi.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Wifi</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/laundry.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Laundry</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>

<!--Room & Rates Start-->
<section class="roomRate">
  <div class="container">
    <div class="title text-center">
      <h2>Room & Rate</h2>
      <p> <span>Find the room for your unforgettable stay</span></p>
    </div>
    <div class="service_block">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" href="#droom" role="tab" data-toggle="tab">Deluxe Room</a> </li>
        <li class="nav-item"> <a class="nav-link" href="#troom" role="tab" data-toggle="tab">Triple Bed Room</a> </li>
        <li class="nav-item"> <a class="nav-link" href="#dbroom" role="tab" data-toggle="tab">Doubled Bed Room</a> </li>
      </ul>
      
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane slide in active" id="droom">
          <div class="row">
            <div class="col-lg-6"> <img src="img/pexels-photo-164595-840x560.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Deluxe Room</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Breakfast</li>
                <li>Complimentary Wifi</li>
                <li>Complimentary local calls and long-distance calls</li>
              </ul>
              <p class="price">Price start at: <b>$116</b> for night</p>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="troom">
          <div class="row">
            <div class="col-lg-6"> <img src="img/pexels-photo-237393-840x560.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Triple Bed Room</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Breakfast</li>
                <li>Complimentary Wifi</li>
                <li>Complimentary local calls and long-distance calls</li>
              </ul>
              <p class="price">Price start at: <b>$116</b> for night</p>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="dbroom">
          <div class="row">
            <div class="col-lg-6"> <img src="img/pexels-photo-271655-840x560.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Doubled Bed Room</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Breakfast</li>
                <li>Complimentary Wifi</li>
                <li>Complimentary local calls and long-distance calls</li>
              </ul>
              <p class="price">Price start at: <b>$116</b> for night</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Room & Rates End--> 
<!--Testimonial Start-->
<section class="testimonial-wrapper parallax">
  <div class="container">
  <div class="title text-center whiteText">
      <h2>Testimonal</h2>
     
    </div>
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="owl-carousel owl-theme testimonial">
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonial End--> 
<div class="top-footer">
    	<div class="container custom-container clearfix">
        	<h3 class="float-left">Follow us through social medai</h3>
            <ul class="float-right social_media">
            	<li><a href="#"><i class="fa fa-facebook"></i><span><b>3,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i><span><b>1,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-tripadvisor"></i><span><b>2,060</b>Followers</span></a></li>
            </ul>
        </div>
    </div>
<?php include('footer.php')?>
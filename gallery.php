<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Gallery</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    
    <div class="masonry-grid row">
								
								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/22303168.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/22303168.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/68463454-hotel-wallpapers.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/68463454-hotel-wallpapers.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/ghalelgaun.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/ghalelgaun.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/mardi-himal-trek1.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/mardi-himal-trek2.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/pexels-photo-164595-840x560.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/pexels-photo-164595-840x560.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
                                <!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/pexels-photo-237393-840x560.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/pexels-photo-237393-840x560.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/slider-one.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/slider-one.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/venetian_resort_hotel_casino_las_vegas-wide.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/venetian_resort_hotel_casino_las_vegas-wide.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								
							

							</div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>
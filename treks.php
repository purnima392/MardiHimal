<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Treks</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Treks</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="img/Mardi Himal Trekking.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="img/mardi-himal-trekking.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="img/mardi-himal-trekking.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
          
          <div class="btn-wrap clearfix"> <a href="contact.php" class="enquirebtn">Enquiry Us</a> <a href="booking-form.php" class="bookbtn">Book This Trip</a> </div>
          
          <!-- tabs start -->
          <div class="tabs-style treks_detail"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li  class="nav-item"><a href="#h2tab1" class="nav-link active" role="tab" data-toggle="tab">Trip Overview</a></li>
              <li class="nav-item"><a href="#h2tab2" class="nav-link" role="tab" data-toggle="tab">Detail Itenary</a></li>
              <li class="nav-item"><a href="#h2tab3" class="nav-link" role="tab" data-toggle="tab">Inclusions & Exclusions</a></li>
              <li class="nav-item"><a href="#h2tab4" class="nav-link" role="tab" data-toggle="tab">Trip Facts</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="h2tab1">
                <p>The Annapurna Circuit in Nepal is one of the world's classic multiday hikes. While the start and stop points and route can be variable, in general trekkers go from Besisahar to Nayapul, roughly 134 miles in total.</p>
                <p>The Annapurna Circuit was once billed as one of the most beautiful treks in the world - it was a 'must' item on many adventurer's bucket list. But things have changed over the the last few years. Namely road building. Ugly road building. The apple growers in the village of Marpha had bushels of apples that were rotting because they couldn't get them to market across the mountainous trails. What to do? Definitely don't ask other villages or think about the impact on the main source of income (trekkers). Soon bulldozers moved in and turned trail into 4x4 jeep track with locals and trekkers zooming up and down the valleys in a gaggle of Indian-made jeeps, a chorus of honks and an encore of dust. It's easy to criticize, but it's also hard to blame villagers for wanting a road to their house, a road that opens up access to food and healthcare and the ability to visit family.</p>
                
              </div>
              <div class="tab-pane fade" id="h2tab2">
                <ul class="itenary-list">
                  <li> <b>Day 1:</b>
                    <h3>Arrival at Pokhara</h3>
                    <p>You will be welcomed on your arrival by our representative at Tourist Buspark or Airport, Pokhara. You will be transferred to your hotel. Once you check into the hotel, find some free and relaxing time until we meet for the briefing at some point in the evening. You will be informed about the time for the briefing at your hotel.</p>
                  </li>
                  <li> <b>Day 2:</b>
                    <h3>Drive from Pokhara to Besishawar</h3>
                    <p>We drive from Pokhara to Besishawar by bus then drive again by bus or Jeep to Chamje.</p>
                    <strong>Approximate Trekking Duration - 5-6 Hrs</strong> </li>
                  <li> <b>Day 3:</b>
                    <h3>Chamje to Dharapani</h3>
                    <p>Another nice walking day today takes you to Bagarchhap to stop for the overnight stay. During the six hours on nice walk, you can see and experience the mountain climate and culture. The more further you walk, the higher you are heading and slow pace will help you to acclimatize.</p>
                    <p>Check into a tea house at Bagarchhap and relax.</p>
                    <strong>Approximate Distance - 12 km</strong> <strong>Approximate Trekking Duration - 6 Hrs</strong> </li>
                  <li> <b>Day 4:</b>
                    <h3>Bagarchhap to Chame </h3>
                    <p>You are heading towards Chame, the headquarters of Manang, the district beyond the Himalayas, as it is famously known as in Nepal. You will, undoubtedly, enjoy your stay in the mountain town in the lap of Annapurna II - 7937 m / 26040 ft.
                      The walk today is about five hours and offers you some breathtaking views.</p>
                    <p>Check into a guest house in Chame and relax.</p>
                    <strong>Approximate Distance - 10 km</strong> <strong>Approximate Trekking Duration - 5 Hrs</strong> </li>
                  <li> <b>Day 5:</b>
                    <h3>Chame to Pisang</h3>
                    <p>A steep and narrow path through a very dense forest will bring us to the dramatic curved rock face, rising 1500m from the river. The forests and heavy vegetation would now be gone. We cross and re-cross the river again and again on suspension bridges. As the trail opens up we get surrounded by majestic Himalayan Peaks. We find ourselves in U-shaped valley of Manang hanging between two giant snow peaks. Upon walking through these wonderful sites we reach at Pisang. Overnight at Upper Pisang.</p>
                    <strong>Approximate Trekking Duration 5 - 6 Hrs</strong> </li>
                  <li> <b>Day 6:</b>
                    <h3>Pisang to Manang</h3>
                    <p>An about five hours trek today takes you to the mountain village of Manang where you can see the prevalent Tibetan Buddhist culture. The walk itself offers a magnificent view of the mountain peaks in the famous Annapurna range. Serene mountain atmosphere is an additional bonus for your walk.</p>
                    <p>Once in Manang, check into a tea house.</p>
                    <strong> Approximate Distance - 9 km</strong> <strong>Approximate Trekking Duration 5 Hrs</strong> </li>
                  <li> <b>Day 7:</b>
                    <h3>Manang Acclimatization Day </h3>
                    <p>Take a day break in Manang to acclimatize to the altitude. Explore the local area of Gangapurna glacier and Gangapurna lake be comfortable adjusting in the high altitude climate.</p>
                    <p>Stay in Manang</p>
                  </li>
                  <li> <b>Day 8:</b>
                    <h3>Manang to Yak Kharka</h3>
                    <p>Head towards Yak Kharka, today's destination after having breakfast. Yak Kharka literally means the Yak pasture land. You come across those mountain giants - yaks - along the trail. Enjoy the mountain view and beautiful atmosphere on your six hours walk today.</p>
                    <p>Once in Yak Kharka, check into a tea house.</p>
                    <strong>Approximate Distance - 8 km</strong> <strong>Approximate Trekking Duration - 6 Hrs</strong> </li>
                  <li> <b>Day 9:</b>
                    <h3>Yak Kharka to Thorong Phedi</h3>
                    <p>After having breakfast in the morning, start your walk of the day to Thorong Phedi - the base of the Thorong-la Pass. A five hours slow paced walk takes you to the place where you are staying overnight today. Enjoy and relax in the cool mountain environment.</p>
                    <strong>Approximate Distance - 5 km</strong> <strong>Approximate Trekking Duration - 5 Hrs</strong> </li>
                  <li> <b>Day 10:</b>
                    <h3>Thorong Phedi to Thorong pass</h3>
                    <p>Today is a long day of walk and the highlight of this trip. Wake up very early in the morning to start your adventure towards the highest pass - Thorong pass. A slow paced up hill walk takes you about 4-5 hours to reach the pass. When you are at the top, you feel like you have conquered everything and the view from there makes you forget all the effort and difficulties you went through to make it there. You have conquered the highlight of this trip. Now, you walk starts descending towards Muktinath for about another six hours.</p>
                    <p>Once in Muktinath, check into a tea house and relax.</p>
                    <strong>Approximate Distance - 15 km</strong> <strong>Approximate Trekking Duration 10 - 11 Hrs</strong> </li>
                  <li> <b>Day 11:</b>
                    <h3>Muktinath to Marpha</h3>
                    <p>Muktinath is an important pilgrimage for both Hindus and Buddhists. In the morning, we pay our visit to Vishnu Temple and Gompa. Descending from Ranipauwa village down the steep and barren hillside, we tumble down toward Kagbeni and then to Jomsom finally arriving at Marpha. The trail today is quite surreal as we trek along a plateau above Kali Gandaki, the world`s deepest gorge. The barren landscape of this area resembles Tibet. Marpha is also famous as the apple capital of Nepal where one can enjoy different items made from apple. The local apple brandy of Marpha is famous all over Nepal. </p>
                    <p>Stay in a hotel in Marpha.</p>
                    <strong>Approximate Trekking Duration  4-5 Hrs</strong> </li>
                  <li> <b>Day 13:</b>
                    <h3>Marpha to Kalopani</h3>
                    <p>From Marpha, we take a new route toward Kalopani via Chokhopani village. Today, we come across traditional villages of the ethnic Thakali people. Also, we get to see apple gardens. Waiting here is the 360 degrees panorama of Himalayan peaks: Dhaulagari, Tukuche Peak, the three Nilgiris, Fang and Annapurna I. From Chokhopani, we continue to Kokhethanti. Upon crossing a river, we come across the newly constructed road site before reaching Kokhethanti of Kalopani. </p>
                    <p>Stay in a hotel in Kalopani.</p>
                    <strong>Approximate Trekking Duration  5-6 Hrs</strong> </li>
                  <li> <b>Day 14:</b>
                    <h3>Kalopani to Tatopani</h3>
                    <p>Again to avoid the new roads, we take a new route to Tatopani from Kalopani. The journey is mostly downhill. We cross the bridge at Ghasa. As we drop to lower elevations, we emerge back into subtropical forests, lush with vegetation. We continue along Rupse Chahara (popular waterfall). We avoid the road and continue down the east bank from Kopchepani via Garpar to a bridge at Dana. At Narchyang Besi, we get to see a powerhouse that supplies electricity in the area. We experience more villages in this area where we can observe the everyday lives of the local people. Upon reaching Tatopani, we relax and have a bath in the hot spring.</p>
                    <strong>Approximate Trekking Duration  6-7 Hrs</strong> </li>
                  <li> <b>Day 15:</b>
                    <h3>Tatopani to Ghorepani</h3>
                    <p>While walking, we observe the lifestyle of the people in the midland villages of Nepal. Ghara and Sikha are the villages with terraced and inclined farmlands. We gradually walk steep up to ascent Ghorepani. While traversing through the Phthalate, Chitre and a no-habitation area, we encounter with rhododendron, birch, magnolia and some meadows on the way. As we gain height, the peaks ahead look wonderful. We stay at Ghorepani for we have to make the climb to Poon Hill early tomorrow morning. </p>
                    <p>Overnight at Ghorepani.</p>
                    <strong>Approximate Trekking Duration  7-8 Hrs</strong> </li>
                  <li> <b>Day 16:</b>
                    <h3>Ghorepani to Tadapani</h3>
                    <p>Early at dawn, we ascend to Poon Hill (3,210m. /10,531ft) to catch the moment of spectacular sunrise over the whole Annapurna and Dhaulagiri massifs and surrounding rice terraces. Nick name of Poon Hill is itself a photographer`s paradise. Upon breakfast, we continue ups and downs trail trek to Tadapani for overnight through the rhododendron forests. We like to keep inspecting at the horizon as sceneries are equally dazzling throughout the day at the trek.</p>
                    <strong>Approximate Trekking Duration  6-7 Hrs</strong> </li>
                  <li> <b>Day 16:</b>
                    <h3>Tadapani to Ghandruk to Pokhara</h3>
                    <p>The trail leads all the way down through the Rhododendron forest to Ghandruk. This is the second-largest Gurung settlement in Nepal. Upon our lunch, we continue trek to Nayapul to complete the trail which is followed by a short bus ride back to Pokhara, a peaceful lakeside city harboring a unique setting of tropical climate and vegetation with the dramatic backdrop of the Himalayas.</p>
                    <strong>Approximate Trekking Duration  5-6 Hrs, 1 Hr Drive</strong> </li>
                </ul>
              </div>
              <div class="tab-pane fade" id="h2tab3"> <strong>The Trip cost will be vary depending on the Group size duration of days and Services required please contact us Via our email <a href="#">shivamardihimal@gmail.com</a> with your details to obtain a quote.</strong>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Includes:</h3>
                    <ul class="list-icons">
                      <li>Accommodation in a (3) three Star hotels in the cities but for the duration of the trek only simple lodges are available.</li>
                      <li>All  ground Transportation</li>
                      <li>Sightseeing in pokhara where Earthbound Expeditions city tour guide will accompany you.</li>
                     <li>Full broad meals will be provided throughout the trek.</li> 
                      <li>Government trained guides and needed no. of porters with their insurance, salary and meals will be provided.</li>
                      <li>Farewell dinner and along with one cultural show.</li>
                      <li>Trek entry fees and TIMS permit will be provided.</li>
                      <li>Domestic Flights will be booked according to the Itinerary listed.</li>
                     <li>Sleeping bags and down jackets for trek will be provided.</li>
                      <li>Car / Bus / Domestic Flights included as per itinerary </li>
                      <li>First Aid kit</li>
                     
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h3>Excludes:</h3>
                    <ul class="list-icons">
                      <li>International Flight tickets.</li>
                      
                      <li>All personal expenses such as bar bills, beverage, snacks etc.</li>
                     <li>Extra shower, battery re-charge, laundry charge etc.</li> 
                      <li>Personal clothing and gears.</li>
                      <li>Tips to guide and porter.</li>
                      <li>Cost raised by cancellation, landslide, weather, political unrest, illness will not be our responsibility since these factors are not within our grasp.</li>
                      
                    </ul>
                  </div>
                </div>
                </div>
                <div class="tab-pane fade" id="h2tab4">
                <div class="facts-module">
								
									<div class="box">
										<ul>
	<li>
		<span class="dt">country</span>
		<span class="dd">Nepal</span>
	</li>
	<li>
		<span class="dt">Destination</span>
		<span class="dd">Throungla pass( longest pass in the world)</span>
	</li>
	<li>
		<span class="dt">Duration</span>
		<span class="dd">18 days</span>
	</li>
	<li>
		<span class="dt">Activities</span>
		<span class="dd">Cultural tour/Trekking</span>
	</li>
	<li>
		<span class="dt">Grade</span>
		<span class="dd">
			<span class="text">Moderate  and Fairly Strenuous <a href="#" data-toggle="modal" data-target="#myModal">Learn</a>
            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      <div class="grade"> <div class="modal-title">
       	<h3>Grade A: Easy *</h3>
       </div>
       <p>Trekking is essentially day hiking and doesn't require any special training. However, good physical condition, a love of walking, and a desire to enjoying the spectacular views of the mountains and encounter village life are essential. We offer a diverse range of easy treks. Categorizing a trek as easy means that no difficult climbing or ascents to high altitudes is involved. They take usually no more than a week and are suitable for anyone. Be assured that a loss of altitude in no way means a loss of interesting things to see and experience. While our more challenging treks get you closer to a small number of mountain ranges, lower altitude treks often provide better viewpoints from which to enjoy the colorful horizons of a whole series of ranges. The duration of a trek can be from 4 to 9 days with an average of 4 to 5 hours walking per day. The elevation of the trail will be between 800m/2624ft and 2800m/ 9240ft above sea level.</p></div>
       <div class="grade"> <div class="modal-title">
       	<h3>Grade B: Moderate **</h3>
       </div>
       <p>These treks are suitable for any walker looking for something a little more challenging and energetic. They are a combination of some longer and shorter walks and hill-walking experience is desirable. The duration is usually from 10 to 15 days. Following the up and down terrain of Nepal and walking to higher elevations contrasts these treks to those in the easy classification. However, you will be rewarded for your efforts with spectacular close-up views of glaciers and of the high Himalayas. Although the terrain is not difficult, some vigorous hiking experience is useful. There may be up to 6 hours a day on the trail and the elevation rises and falls from 800m/ 2624ft to 4000m/13210ft above sea level.</p></div>
       <div class="grade"> <div class="modal-title">
       	<h3>Grade C: Fairly Strenuous ***</h3>
       </div>
       <p>Since the terrain can be hard and the days long, hikers on these treks should be in good physical condition and have some previous mountain walking experience. Steep climbing may be involved, although it is never necessary to use ropes. Treks at this level can he arranged for periods of 16 to 21 days. Typically, a gradual ascent through a green river valley will lead you up to a number of high passes, where you will reach the altitude of 5416m. Often times, you will get a close insight into the Tibetan culture. Participants should except to trek above 5416m/17872ft.</p></div>
       <div class="grade"> <div class="modal-title">
       	<h3>Grade D: Strenuous ****</h3>
       </div>
       <p>These real adventure treks are both technical and highly strenuous. Excellent physical condition is essential and mountaineering experience is preferable. Following rough terrain, they involve steep ascents to high altitudes with the possibility of some rope climbing. Stamina is needed to complete one of these treks, as it can take from 20 to 28 days to reach the heart of the wildernesses that they transverse. Participants should except to trek above 5600m/18480ft</p></div>
       <div class="grade"> <div class="modal-title">
       	<h3>NOTE</h3>
       </div>
       <p>Please note that the grade of treks can vary throughout the year as weather and ground conditions change. These can vary on a day-to-day basis as well as season to season. You should be aware of this and take it into consideration when you choose the level of trek that is suitable for you.</p></div>
      </div>
      
      
    </div>
  </div>
</div>
            </span>
		</span>
	</li>
	<li>
		<span class="dt">Best Season</span>
		<span class="dd">February to May &amp; Sep to second week of the December</span>
	</li>
	<li>
		<span class="dt">Group Size</span>
		<span class="dd">Min. 2 and Max 15</span>
	</li>
	<li>
		<span class="dt">Altitude</span>
		<span class="dd">Min. 820m/Max. 5416m</span>
	</li>
	<li>
		<span class="dt">Starts/Ends</span>
		<span class="dd">Pokhara</span>
	</li>
	<li>
		<span class="dt">Accommodation</span>
		<span class="dd">3 to 4 stars hotel in Pokhara on BB plan &amp; mountain lodge( Tea-house ) in trekking.</span>
	</li>
	<li>
		<span class="dt">Meals included</span>
		<span class="dd">Breakfast and welcome or farewell dinner in Pokhara / Full board in Trekking (Breakfast, Lunch &amp; Dinner).</span>
	</li>
	<li>
		<span class="dt">Transportation</span>
		<span class="dd">Airport / Hotel / Airport pick up &amp; drop by private car- By bus from Pokhara to Besishar and by car from trek ending point to Pokhara.</span>
	</li>
</ul>									</div>
								</div>
                
                </div>
            </div>
          </div>
          <!-- tabs end --> 
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>
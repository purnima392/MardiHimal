<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Room & Rates</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Room & Rates</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
  	<div class="row">
    	<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/22303168.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Deluxe Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/pexels-photo-237393-840x560.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Triple Bed Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/pexels-photo-271655-840x560.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Doubled Bed Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/22303168.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Deluxe Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/pexels-photo-237393-840x560.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Triple Bed Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/pexels-photo-271655-840x560.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Doubled Bed Room</a></h3>
					<span class="price">$35.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>